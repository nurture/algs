The 'Algs' system.

Usage:
Current main class is de.arudolph.stacktest.StackTest. It represents the
Stack.

commands:
'u' - push
'o' - pop
'e' - test for emptyness
'q' - quit (important!)

The last command 'q' is important because the program informs other nodes that
it's gone so that they don't attempt to create workers in it.

The code is complicated because I haven't created any API and all the
unnecessary implementation details stay visible. Any attempts to hide them are
welcomed.

If you want to implement your algorithm, create some package and place your
code in it, commit code, others will sync their projects with repo and then
will run the main class.
