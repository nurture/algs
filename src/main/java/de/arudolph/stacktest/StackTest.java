package de.arudolph.stacktest;

import java.util.Scanner;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;

public class StackTest {
	public static void main(String[] args) throws Exception {
		ActorSystem sys = ActorSystem.apply("test");
		String link = sys.provider().getDefaultAddress().toString();
		sys.actorOf(Props.create(LinksProvider.class, link), "linksProvider");
		ActorRef testActor = sys.actorOf(Props.create(TestActor.class), "myTestActor");
		testActor.tell("START", null);
		Scanner in = new Scanner(System.in);
		System.out.println("commands:\n   'u' - push\n   'o' - pop\n   'e' - test for emptyness\n   'q' - quit (important!)");
		while (true) {
			String line = in.nextLine();
			if ("q".equals(line)) {
				in.close();
				sys.shutdown();
				break;
			}
			testActor.tell(line, null);
		}
	}
	
	static class TestActor extends UntypedActor {
		ActorRef ref;
		int counter;
		@Override
		public void onReceive(Object msg) throws Exception {
			if ("START".equals(msg)) {
				ref = getContext().actorOf(Props.create(StackActor.class), "myStackActor");
				ref.tell(new StackActor.IsEmpty(), getSelf());
				ref.tell(new StackActor.Push<Integer>(counter++), getSelf());
				ref.tell(new StackActor.IsEmpty(), getSelf());
				ref.tell(new StackActor.Push<Integer>(counter++), getSelf());
				ref.tell(new StackActor.Push<Integer>(counter++), getSelf());
				ref.tell(new StackActor.Push<Integer>(counter++), getSelf());
				ref.tell(new StackActor.Pop(), getSelf());
				ref.tell(new StackActor.Pop(), getSelf());
				ref.tell(new StackActor.Pop(), getSelf());
				ref.tell(new StackActor.Push<Integer>(counter++), getSelf());
				ref.tell(new StackActor.Push<Integer>(counter++), getSelf());
				ref.tell(new StackActor.Push<Integer>(counter++), getSelf());
				ref.tell(new StackActor.Pop(), getSelf());
				ref.tell(new StackActor.Pop(), getSelf());
			}
			else if ("u".equals(msg)) {
				ref.tell(new StackActor.Push<Integer>(counter++), getSelf());
			}
			else if ("o".equals(msg)) {
				ref.tell(new StackActor.Pop(), getSelf());
			}
			else if ("e".equals(msg)) {
				ref.tell(new StackActor.IsEmpty(), getSelf());
			}
			else {
				System.out.println(msg);
			}
		}
	}
}
