package de.arudolph.stacktest;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;

public class NodeActor<Item> extends UntypedActor {
	public static class GetItem {}
	public static class ItemResult<Item> {
		public Item item;
		public ItemResult(Item item) {
			this.item = item;
		}
	}
	public static class GetNext {}
	public static class NextResult {
		public ActorRef value;
		public NextResult(ActorRef next) {
			this.value = next;
		}
	}
	public static class GetNextAndItem {}
	public static class NextAndItemResult<Item> {
		public Item item;
		public ActorRef next;
		public NextAndItemResult(Item item, ActorRef next) {
			this.item = item;
			this.next = next;
		}
	}
	public static class SetItem<Item> {
		public Item item;
		public SetItem(Item item) {
			this.item = item;
		}
	}
	public static class SetNext {
		public ActorRef next;
		public SetNext(ActorRef next) {
			this.next = next;
		}
	}
	
	private Item item;
	private ActorRef next;
	
	public NodeActor(Item item, ActorRef next) {
		this.item = item;
		this.next = next;
	}

	@SuppressWarnings("unchecked")
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof GetItem) {
			getSender().tell(new ItemResult<Item>(item), getSelf());
		}
		else if (msg instanceof SetItem) {
			item = ((SetItem<Item>)msg).item;
		}
		else if (msg instanceof GetNext) {
			getSender().tell(new NextResult(next), getSelf());
		}
		else if (msg instanceof SetNext) {
			next = ((SetNext)msg).next;
		}
		else if (msg instanceof GetNextAndItem) {
			getSender().tell(new NextAndItemResult<Item>(item, next), getSelf());
		}
		else unhandled(msg);
	}
	
}
