package de.arudolph.stacktest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.core.MultivaluedMap;

import akka.camel.CamelMessage;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class LinksProvider extends akka.camel.javaapi.UntypedConsumerActor {
	private static List<String> links = new ArrayList<String>();
	private static Random random = new Random();
	private String address;
	
	public LinksProvider(String address) {
		this.address = address;
		sayHi();
	}
	
	public static String link() {
		String link = null;
		if ( ! links.isEmpty()) link = links.get(random.nextInt(links.size()));
		System.out.println(">>>link>>> " + link);
		return link;
	}

	private void sayHi() {
		Client client = Client.create();
		WebResource webResource = client.resource(
				"http://1-dot-alglinksprovider.appspot.com/links");

		MultivaluedMap<String, String> formData = new MultivaluedMapImpl();
		formData.add("link", address);
		ClientResponse response = webResource.type("application/x-www-form-urlencoded").post(ClientResponse.class, formData);
		if (response.getStatus() != 200) {
			throw new RuntimeException(
					"Failed. code=" + response.getStatus());
		}
		String links = response.getEntity(String.class);
		System.out.println(">>>>returned links>>> \"" + links + "\"");
		System.out.println(">>>>returned links_size>>> \"" + links.split("\\s").length + "\"");
		List<String> asList = Arrays.asList(links.split("\\s"));
		LinksProvider.links = new ArrayList<String>(asList);
		LinksProvider.links.remove("");
	}
	
	@Override
	public void postStop() throws Exception {
		sayImGone();
		super.postStop();
	}
	
	private void sayImGone() {
		Client client = Client.create();
		WebResource webResource = client.resource(
				"http://1-dot-alglinksprovider.appspot.com/links");

		MultivaluedMap<String, String> formData = new MultivaluedMapImpl();
		formData.add("link", address);
		formData.add("gone", "true");
		ClientResponse response = webResource.type("application/x-www-form-urlencoded").post(ClientResponse.class, formData);
		
		if (response.getStatus() != 200) {
			throw new RuntimeException(
					"Failed. code=" + response.getStatus());
		}
	}

	@SuppressWarnings("serial")
	private static class BadLinkException extends Exception {

		public BadLinkException(String link) {
			super(link);
		}
		
	}
	private String extractHost(String link) throws BadLinkException {
		Pattern p = Pattern.compile("akka.tcp://\\w+@([0-9.]+):\\d*");
		Matcher m = p.matcher(link);
		if (!m.matches()) {
			throw new BadLinkException(link);
		}
		return m.group(1);
	}

	@Override
	public String getEndpointUri() {
		try {
			String host = extractHost(address);
			return "jetty:http://" + host + ":8877/camel/default";
		} catch (BadLinkException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void onReceive(Object msg) throws Exception {
		if (msg instanceof CamelMessage) {
			Map<String, Object> headers = ((CamelMessage)msg).getHeaders();
			String link = (String) headers.get("link");
			String was = (String) headers.get("was");
			if ("gone".equals(was)) {
				LinksProvider.links.remove(link);
			} else if ("added".equals(was)) {
				LinksProvider.links.add(link);
			}
		}
	}
	
}
