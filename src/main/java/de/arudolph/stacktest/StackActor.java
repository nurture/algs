package de.arudolph.stacktest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import scala.concurrent.Future;
import akka.actor.ActorRef;
import akka.actor.AddressFromURIString;
import akka.actor.Deploy;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.japi.Procedure;
import akka.pattern.Patterns;
import akka.remote.RemoteScope;
import akka.util.Timeout;

public class StackActor<Item> extends UntypedActor {
	public static class Push<Item> {
		public Item value;
		public Push(Item value) {
			this.value = value;
		}
	}
	public static class Pop {}
	public static interface IPopResult {}
	public static class PopResult<Item> implements IPopResult {
		public Item value;
		public PopResult(Item value) {
			this.value = value;
		}
		public String toString() {
			return "PopResult [value=" + value + "]";
		}
	}
	public static class StackIsEmpty implements IPopResult {}
	public static class IsEmpty {}
	public static class IsEmptyResult {
		public boolean value;
		public IsEmptyResult(boolean value) {
			this.value = value;
		}
		public String toString() {
			return "IsEmptyResult [value=" + value + "]";
		}
	}
	
	private ActorRef first;
	private int size;
	
	public StackActor() {
		getContext().become(normal);
	}
	
	@Override
	public void onReceive(Object arg0) throws Exception {}
	
	private ActorRef createNode(Item item, ActorRef next) {
		Props props = Props.create(NodeActor.class, item, first);
		String link = LinksProvider.link();
		if (link != null) {
			props = props.withDeploy(new Deploy(new RemoteScope(AddressFromURIString.apply(link))));
		}
		ActorRef result = getContext().actorOf(props);
		return result;
	}

	private Procedure<Object> normal = new Procedure<Object>() {
		public void apply(Object msg) throws Exception {
			if (msg instanceof Push) {
				@SuppressWarnings("unchecked")
				Item item = ((Push<Item>)msg).value;
				first = createNode(item, first);
				size++;
			}
			else if (msg instanceof Pop) {
				if (size == 0) {
					getSender().tell(new StackIsEmpty(), getSelf());
					return;
				}
				waitingNextAndItemResult.setClient(getSender());
				getContext().become(waitingNextAndItemResult);
				Timeout timeout = new Timeout(2, TimeUnit.SECONDS);
				Future<Object> futureNext = Patterns.ask(first, new NodeActor.GetNextAndItem(), timeout);
				Patterns
					.pipe(futureNext, getContext().system().dispatcher())
					.to(getSelf());
				size--;
			}
			else if (msg instanceof IsEmpty) {
				getSender().tell(new IsEmptyResult(size == 0), getSelf());
			}
			else unhandled(msg);
		}
	};
	
	private WaitingNextAndItemResult waitingNextAndItemResult = new WaitingNextAndItemResult();

	private class WaitingNextAndItemResult implements Procedure<Object> {
		private class MsgAndSender {
			Object msg;
			ActorRef sender;
			public MsgAndSender(Object msg, ActorRef sender) {
				this.msg = msg;
				this.sender = sender;
			}
		}
		private List<MsgAndSender> msgs = new ArrayList<MsgAndSender>();
		private ActorRef client;

		public void setClient(ActorRef client) {
			this.client = client;
		}

		public void apply(Object msg) throws Exception {
			if (msg instanceof NodeActor.NextAndItemResult) {
				@SuppressWarnings("unchecked")
				NodeActor.NextAndItemResult<Item> nextAndItem = (NodeActor.NextAndItemResult<Item>)msg;
				first.tell(PoisonPill.getInstance(), getSelf());
				first = nextAndItem.next;
				client.tell(new PopResult<Item>(nextAndItem.item), getSelf());
				client = null;
				getContext().become(normal);
				for (MsgAndSender pair : msgs) {
					getSelf().tell(pair.msg, pair.sender);
				}
				msgs.clear();
			}
			else {
				msgs.add(new MsgAndSender(msg, getSender()));
			}
		}
	};

}
