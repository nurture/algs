//package de.kimrudolph.akkaflow.actors;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Scope;
//import org.springframework.stereotype.Component;
//
//import akka.actor.PoisonPill;
//import akka.actor.UntypedActor;
//import akka.event.Logging;
//import akka.event.LoggingAdapter;
//import de.kimrudolph.akkaflow.algs.stack.node.ActorRefHolderStackNode;
//import de.kimrudolph.akkaflow.beans.Task;
//
//@Component
//@Scope("prototype")
//public class TaskActor extends UntypedActor {
//
//	public static class SetNextStackNode<Item> {
//
//		private ActorRefHolderStackNode<Item> stackNode;
//
//		public SetNextStackNode(ActorRefHolderStackNode<Item> stackNode) {
//			this.stackNode = stackNode;
//		}
//		public ActorRefHolderStackNode<Item> getStackNode() {
//			return stackNode;
//		}
//	}
//	public static class GetNextStackNode {}
//	public static class NextStackNodeResult<Item> {
//		private ActorRefHolderStackNode<Item> stackNode;
//
//		public NextStackNodeResult(ActorRefHolderStackNode<Item> stackNode) {
//			this.stackNode = stackNode;
//		}
//		public ActorRefHolderStackNode<Item> getStackNode() {
//			return stackNode;
//		}
//	}
//	public static class GetStackNodeItem {}
//	public static class StackNodeItemResult<Item> {
//		private Item item;
//		public StackNodeItemResult(Item item) {
//			this.item = item;
//		}
//		public Item getItem() {
//			return item;
//		}
//	}
//    private final LoggingAdapter log = Logging
//        .getLogger(getContext().system(), "TaskProcessor");
//
//    //@Autowired
//    //private TaskDAO taskDAO;
//    private ActorRefHolderStackNode stackNode;
//
//    @Override
//    public void onReceive(Object message) throws Exception {
//    	
//    	if (message instanceof GetNextStackNode) {
//    		getSender().tell(new NextStackNodeResult((ActorRefHolderStackNode)stackNode.next()), getSelf());
//    	} else if (message instanceof SetNextStackNode) {
//    		if (stackNode.next() != null) 
//    			((ActorRefHolderStackNode)stackNode.next()).ref()
//    				.tell(PoisonPill.getInstance(), getSelf());
//    		ActorRefHolderStackNode next = ((SetNextStackNode)message).getStackNode();
//    		stackNode.setNext(next);
//    	} else if (message instanceof GetStackNodeItem) {
//    		getSender().tell(new StackNodeItemResult(stackNode.item()), getSelf());
//    	}
//
//        //Long result = taskDAO.createTask((Task) message);
//        //log.debug("Created task {}", result);
//    }
//}
